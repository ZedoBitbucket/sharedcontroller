﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SharedController.Models
{
	public class Wniosek
	{
		public Wniosek()
		{
			Sekcja1 = new Sekcja1();
			Sekcja2 = new Sekcja2();
			Sekcja3 = new Sekcja3();
		}
		public Sekcja1 Sekcja1 { get; set; }
		public Sekcja2 Sekcja2 { get; set; }
		public Sekcja3 Sekcja3 { get; set; }
	}

	public class Sekcja
	{
		public Sekcja()
		{
			Osoba=new Osoba();
		}
		public string Info { get; set; }
		public Osoba Osoba { get; set; }
	}

	public class Osoba
	{
		public string Name { get; set; }
		public int Age { get; set; }
	}

	public class Sekcja1 : Sekcja
	{
		public Sekcja1()
		{
			Info = "Sekcja 1";
			Osoba = new Osoba(){Age = 25, Name = "Tomek"};
		}
	}
	public class Sekcja2 : Sekcja
	{
		public Sekcja2()
		{
			Info = "Sekcja 2";
			Osoba=new Osoba(){Age = 35, Name = "Krystyna"};
		}
	}
	public class Sekcja3 : Sekcja
	{
		public Sekcja3()
		{
			Info = "Sekcja 3";
			Osoba=new Osoba(){Age = 150, Name = "Ania"};
		}
	}



}