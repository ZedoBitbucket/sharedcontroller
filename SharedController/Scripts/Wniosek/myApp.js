﻿var myApp = angular.module('myApp', []);

myApp.controller('main', function ($scope, $rootScope) {
    $scope.Info = "main";

    $rootScope.Wniosek = window.Wniosek;

    $scope.GetWniosek= function(sekcja) {
        return JSON.stringify(sekcja, null, 3);
    }

});

/* - 01 - */
myApp.controller('Sekcja1', function ($scope, $rootScope) {


    $scope.Info = $rootScope.Wniosek.Sekcja1.Info;
    $scope.Osoba = $rootScope.Wniosek.Sekcja1.Osoba;


});
/* - 02 - */
myApp.controller('Sekcja2', function ($scope, $rootScope) {

    $scope.ModyfikacjaSekcja3 = function () {
        $rootScope.Wniosek.Sekcja3.Osoba = { Name: 'Fiona', Age: 27 };
    };
    $scope.ModyfikacjaSekcja3ByValue = function () {
        $rootScope.Wniosek.Sekcja3.Osoba.Name = 'Fiona';
        $rootScope.Wniosek.Sekcja3.Osoba.Age = 27;
    };

//    $scope.Info = $rootScope.Wniosek.Sekcja2.Info;
//    $scope.Osoba = $rootScope.Wniosek.Sekcja2.Osoba;


});
/* - 03 - */
myApp.controller('Sekcja3', function ($scope, $rootScope) {

    var obj = { Name: 'Frida (Kopia)', Age: -666 };

    $scope.CopyObj = function() {
        $scope.s.Osoba = $.extend(true, {}, obj);
    };
    $scope.CopyObjRootScope = function () {
        $rootScope.Wniosek.Sekcja3.Osoba = $.extend(true, {}, obj);
    };

//    $scope.Info = $rootScope.Wniosek.Sekcja3.Info;
//    $scope.Osoba = $rootScope.Wniosek.Sekcja3.Osoba;


});